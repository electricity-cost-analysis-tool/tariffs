'''
This module holds samples of the volume component of tariffs structured in a format compatible with
HBin. All tariffs should have length of 52*7*48.
'''


'''
An example of a made up electricity tariff.
'''
sample_volume_tariff_1 = {
    'label': 'year',
    'width': 52,
    'value': {
        'label': 'week',
        'width': 7,
        'children':
        [
            {
                'label': 'weekday',
                'width': 5,
                'value':
                {
                    'label': 'day',
                    'width': 48,
                    'children':
                    [
                        {'label': 'off', 'width': 16, 'value': 1},
                        {'label': 'on', 'width': 16, 'value': 2},
                        {'label': 'off', 'width': 16, 'value': 1},
                    ]
                }
            },
            {
                'label': 'weekend',
                'width': 2,
                'value':
                {
                    'label': 'day',
                    'width': 48,
                    'children':
                    [
                        {'label': 'off', 'width': 16, 'value': 1 / 2},
                        {'label': 'on', 'width': 16, 'value': 2 / 2},
                        {'label': 'off', 'width': 16, 'value': 1 / 2},
                    ]
                }
            }
        ]
    }
}


'''
Alinta Fair Deal 43
'''
alinta_fair_deal_43_summer_week = {
    'label': 'week',
    'width': 7,
    'children':
    [
        {
            'label': 'weekday',
            'width': 5,
            'value':
            {
                'label': 'day',
                'width': 48,
                'children':
                [
                    {'label': 'off', 'width': 14, 'value': 0.2248},
                    {'label': 'shoulder', 'width': 16, 'value': 0.2645},
                    {'label': 'on', 'width': 12, 'value': 0.4786},
                    {'label': 'shoulder', 'width': 2, 'value': 0.2645},
                    {'label': 'off', 'width': 4, 'value': 0.2248},
                ]
            }
        },
        {
            'label': 'weekend',
            'width': 2,
            'value':
            {
                'label': 'day',
                'width': 48,
                'children':
                [
                    {'label': 'off', 'width': 14, 'value': 0.2248},
                    {'label': 'shoulder', 'width': 30, 'value': 0.2645},
                    {'label': 'off', 'width': 4, 'value': 0.2248},
                ]
            }
        }
    ]
}
alinta_fair_deal_43_winter_week = {
    'label': 'week',
    'width': 7,
    'children':
    [
        {
            'label': 'weekday',
            'width': 5,
            'value':
            {
                'label': 'day',
                'width': 48,
                'children':
                [
                    {'label': 'off', 'width': 14, 'value': 0.2248},
                    {'label': 'shoulder', 'width': 16, 'value': 0.2610},
                    {'label': 'on', 'width': 12, 'value': 0.3646},
                    {'label': 'shoulder', 'width': 2, 'value': 0.2610},
                    {'label': 'off', 'width': 4, 'value': 0.2248},
                ]
            }
        },
        {
            'label': 'weekend',
            'width': 2,
            'value':
            {
                'label': 'day',
                'width': 48,
                'children':
                [
                    {'label': 'off', 'width': 14, 'value': 0.2248},
                    {'label': 'shoulder', 'width': 30, 'value': 0.2610},
                    {'label': 'off', 'width': 4, 'value': 0.2248},
                ]
            }
        }
    ]
}
alinta_fair_deal_43_summer = {
    'label': 'summer',
    'width': 13,
    'value': alinta_fair_deal_43_summer_week
}
alinta_fair_deal_43_winter = {
    'label': 'winter',
    'width': 26,
    'value': alinta_fair_deal_43_winter_week
}
alinta_fair_deal_43_volume_charge = {
    'label': 'year',
    'width': 52,
    'children':
    [
        alinta_fair_deal_43_summer,  # 13 weeks 01/01 to 31/03
        alinta_fair_deal_43_winter,  # 26 weeks middle
        alinta_fair_deal_43_summer,  # 13 weeks 10/01 to 31/12
    ]
}
alinta_fair_deal_43 = {
    'name': 'Alinta Fair Deal 43',
    'description': 'A tariff from Alinta',
    'tariff_type': 'TOU',
    'retailer': 'Alinta',
    'temporal_charge': [1.1098],
    'outright_charge': [
        {
            'name': 'connection',
            'charge': 0.0
        }
    ],
    'volume_charge': [
        {
            'name': 'volume',
            'charge': alinta_fair_deal_43_volume_charge
        }
    ]
}


'''
An example of a structured demand charge component from an Origin small business tariff.
Note the charge is divided by the width of the period it applies to. The charge becomes the avg charge
per interval in the period. This makes it easier to calculate in the current implementation.
'''
sample_full_tariff_1_volume_charge = {
    'label': 'year',
    'width': 52,
    'value': {
        'label': 'week',
        'width': 7,
        'children':
        [
            {
                'label': 'weekday',
                'width': 5,
                'value':
                {
                    'label': 'day',
                    'width': 48,
                    'children':
                    [
                        {'label': 'off', 'width': 16, 'value': 0.2248},
                        {'label': 'on', 'width': 16, 'value': 0.3646},
                        {'label': 'off', 'width': 16, 'value': 0.2248},
                    ]
                }
            },
            {
                'label': 'weekend',
                'width': 2,
                'value':
                {
                    'label': 'day',
                    'width': 48,
                    'children':
                    [
                        {'label': 'off', 'width': 16, 'value': 0.2248},
                        {'label': 'on', 'width': 16, 'value': 0.2610},
                        {'label': 'off', 'width': 16, 'value': 0.2248},
                    ]
                }
            }
        ]
    }
}
sample_full_tariff_1_demand_charge = {
    'label': 'year',
    'width': 364,
    'children': [
        {
            'label': 'Summer',
            'width': 90,
            'value': {
                'label': 'day',
                'width': 48,
                'children': [
                    {
                        'label': 'off',
                        'width': 20,
                        'value': 0
                    },
                    {
                        'label': 'on',
                        'width': 16,
                        'value': 0.39666 / 48
                    },
                    {
                        'label': 'off',
                        'width': 12,
                        'value': 0
                    }
                ]
            }
        },
        {
            'label': 'Not Summer',
            'width': 274,
            'value': {
                'label': 'day',
                'width': 48,
                'children': [
                    {
                        'label': 'off',
                        'width': 20,
                        'value': 0
                    },
                    {
                        'label': 'on',
                        'width': 16,
                        'value': 0.26444 / 48
                    },
                    {
                        'label': 'off',
                        'width': 12,
                        'value': 0
                    }
                ]
            }
        },
    ]
}
sample_full_tariff_1 = {
    'name': 'Sample Full Tariff 1',
    'description': 'A business tariff from Origin',
    'tariff_type': 'TOU',
    'retailer': 'Origin',
    'outright_charge': [
        {
            'name': 'connection',
            'charge': 0.0
        }
    ],
    'temporal_charge': [
        {
            'name': 'fixed daily',
            'charge': 0.98 / 48
        }
    ],
    'volume_charge': [
        {
            'name': 'volume',
            'charge': sample_full_tariff_1_volume_charge
        }
    ],
    'demand_charge': [
        {
            'name': 'demand',
            'period': 'month',
            'minimum_demand_charge': 0.0,
            'charge': sample_full_tariff_1_demand_charge
        }
    ],
}
