import numpy as np
from copy import deepcopy
import unittest
from unittest import TestCase
from tariffs import Tariff
from tariffs.interval_tariff_structures import sample_full_tariff_1


class TestTariff(TestCase):

    ''' Test HBin '''

    def test_basics(self):
        tariff = Tariff(sample_full_tariff_1)
        usage = np.ones(tariff._len)
        costs = tariff.apply(usage)
        (outright_costs, temporal_costs) = costs.flat()
        self.assertEqual(type(outright_costs), dict)
        self.assertEqual(temporal_costs.shape, (364 * 48, 3))
