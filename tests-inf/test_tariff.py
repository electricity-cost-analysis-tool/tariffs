import numpy as np
from tariffs import Tariff, Cost
from tariffs.interval_tariff_structures import sample_full_tariff_1


t = Tariff(sample_full_tariff_1)

# Assume thse exist, but they are really optional.
print(t['outright_charge'][0])
print(t['temporal_charge'][0])
print(t['volume_charge'][0], t['volume_charge'][0]['charge'].to_string())
print(t['demand_charge'][0], t['demand_charge'][0]['charge'].to_string())

# Calc some cost.
u = np.ones(364 * 48)
c = t.apply(u)
(oc, tc) = c.flat()
print(oc, tc)
print(tc.sum())
