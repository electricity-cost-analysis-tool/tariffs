import numbers
import pandas
import numpy as np
from copy import deepcopy
from .hbin import HBin
from .interval_tariff_structures import *
from .interval_usage_structures import *


def get_tariff_by_name(name):
    ''' Load a tariff from db of tariffs. Name should be sanitized by client to prevent injection. '''
    return Tariff(eval(name))


class Tariff():
    ''' Simple wrapper over a tariff data structure (dict) passed to this class. Adds some basic
    calculation logic based on a given tariff data structure. A tariff currently has four optional
    components:

        - Out right fixed charges.
        - Fixed temporal charges (example $0.80/day).
        - Volume charges.
        - Demand charges.

    The latter three charges are temporal in that the differ with time. The latter two depend on usage.
    @see interval_tariff_structures
    '''
    _tariff = None
    _volume_charge = None
    _demand_charge = None
    _len = 364 * 48    # Yep, we have a sort of dependency on this number. See _init_tariff().

    def __init__(self, tariff):
        self._tariff = deepcopy(tariff)
        self._init_tariff()

    def __getitem__(self, k):
        return self._tariff[k]

    def __contains__(self, k):
        return k in self._tariff

    def _init_tariff(self):
        ''' Turn complex charges into a HBins. Currently single number fixed daily charges are
        to lists. This is for the convenience of the client of this class so that all temporal charges
        are all lists of the same length (which we have to presume to be 364*48 here).
        @todo Do this JIT.
        @todo Handle single value temporal_charge better than `np.ones(self._len)*c['charge']`
        '''
        if 'temporal_charge' in self:
            for c in self['temporal_charge']:
                c['charge'] = HBin.build(c['charge']) if isinstance(c['charge'], dict) else c['charge']
                c['charge'] = np.ones(self._len) * c['charge'] if isinstance(c['charge'], numbers.Number) else c['charge']    # Hmm.
        if 'volume_charge' in self:
            for c in self['volume_charge']:
                c['charge'] = HBin.build(c['charge']) if isinstance(c['charge'], dict) else c['charge']
        if 'demand_charge' in self:
            for c in self['demand_charge']:
                c['charge'] = HBin.build(c['charge']) if isinstance(c['charge'], dict) else c['charge']

    def apply(self, usage):
        ''' Apply all charges in this tariff wrt to `usage`. Usage is expected to be a list of the correct length. '''
        cost = deepcopy(self._tariff)
        if 'temporal_charge' in cost:
            for c in cost['temporal_charge']:
                c['cost'] = deepcopy(c['charge'])
        if 'volume_charge' in cost:
            for c in cost['volume_charge']:
                c['cost'] = c['charge'] * usage
        if 'demand_charge' in cost:
            for c in cost['demand_charge']:
                c['cost'] = self.apply_demand_charge(c, usage)
        return Cost(cost)

    def apply_demand_charge(self, demand_charge, usage):
        ''' Calculate demand charges based on usage. Not as sraight forward as volume. '''
        period_bins = {
            'day': flat_daily_structure,
            'week': flat_weekly_structure,
            'month': flat_monthly_structure,
            'quarter': flat_quarterly_structure,
        }
        usage = np.array(usage)
        minimum_demand_charge = demand_charge['minimum_demand_charge'] if 'minimum_demand_charge' in demand_charge else 0.
        np.putmask(usage, usage < minimum_demand_charge, minimum_demand_charge)  # Threshold
        cost = demand_charge['charge'] * usage  # Apply the charge. Still need to find the maxs.
        period_bin = HBin.build(period_bins[demand_charge['period']])
        cost = period_bin.binList(cost).maxs()
        return cost


class Cost(Tariff):
        ''' Cost represents a Tariff after usage has been "applied" to it. It's essentially the same as
        as Tariff, but the Cost type encapsulates the contract that costs have been calculated.
        '''
        _tariff = None

        def __init__(self, tariff):
            ''' This should be considered protected and friend only Tariff.apply() '''
            self._tariff = tariff

        def __getitem__(self, k):
            return self._tariff[k]

        def __contains__(self, k):
            return k in self._tariff

        def flat(self):
            ''' Find all costs in hierarchical data structure self.costs and flatten them into DataFrame.
            Outright charges are not time varying so are not passed back in the DataFrame. Instead, outright
            charges are returned in a separate dictionary of <name, value> pairs.
            @todo Code assumes names of outright and all time varying charges have to be globally unique. Silently overrides if not!
            @todo Maybe should just split outright charges over year like other charges for consistency?
            @returns tuple <dict of outright charges, dataframe with temporal charges>
            '''
            outright_charge = {}
            time_varying_charge = pandas.DataFrame()
            for charge_name in ('temporal_charge', 'volume_charge', 'demand_charge'):
                try:
                    for c in self[charge_name]:
                        time_varying_charge[charge_name + '.' + c['name']] = c['cost']
                except KeyError:
                    continue
            if 'outright_charge' in self:
                for c in self['outright_charge']:
                    outright_charge[c['name']] = c['charge']
            return (outright_charge, time_varying_charge)
