# Overview
This package contains hierarchical data structures tha represent both electricity tariffs, and
potential ways electricity usage can be broken down. They can be used by/with [HBin](https://gitlab.com/electricity-cost-analysis-tool/hbin).

Pakage also contains `Tariff` class which is a simple wrapper of a valid tariff data structure.
